const BusinessNetworkConnection     = require('composer-client').BusinessNetworkConnection;
const addParticipant                = require('./addParticipant');
const addAsset                      = require('./addAsset');
const updateAssetValue              = require('./updateAssetValue');
const updateOwner                   = require('./updateOwner');
const CONFIG                        = require('../config.json');
const log                           = require('log-timestamp');

const businessNetworkConnection = new BusinessNetworkConnection();

console.log("registering event listener of the network");
// Event emitted from Business Network Chaincode will be captured here.
businessNetworkConnection.on('event', (event) => {
  console.log("recieved an event");
});

console.log("Attempting to connect to business network");
businessNetworkConnection.connect(CONFIG.card).then(async (businessNetworkDefinition) => {
  console.log("connected to business network");

  const participantID = await addParticipant(businessNetworkConnection, {firstName: "Abhishek", lastName: "Monarch"});
  console.log("created participant with id =>", participantID);

  const assetID = await addAsset(businessNetworkConnection, {ownerID: participantID, value: "50"});
  console.log("created asset with id =>", assetID);

  await updateAssetValue(businessNetworkConnection, businessNetworkDefinition, assetID, "10");
  console.log("will throw error");

  await updateAssetValue(businessNetworkConnection, businessNetworkDefinition, assetID, "100");
  console.log("will update to 100");

  await updateOwner(businessNetworkConnection, businessNetworkDefinition, assetID, "abhishek@box8.in");

  await updateOwner(businessNetworkConnection, businessNetworkDefinition, assetID, "abhishek@gmail.in");

  await updateOwner(businessNetworkConnection, businessNetworkDefinition, assetID, "abhishek@gmail.com");
  
  await businessNetworkConnection.disconnect();
  console.log("disconnected from business network");

  process.exit(0);
}).catch((error) => {
  console.log("error connecting to the network with card => ", CONFIG.card)
  console.log(error);
});


