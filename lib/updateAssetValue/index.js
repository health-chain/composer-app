const CONFIG = require('../../config.json');

module.exports = async function(businessNetworkConnection, businessNetworkDefinition, assetID, newValue){
  try {
    let serializer = businessNetworkDefinition.getSerializer();

    let resource = await serializer.fromJSON({
      '$class': `${CONFIG.namespace}.${CONFIG.transaction}`,
      'asset': assetID,
      'newValue': newValue
    });

    console.log("preparing to submit transaction");
    await businessNetworkConnection.submitTransaction(resource);
    console.log("submitted transaction");
  } catch (error){
    console.log(error);
  }
}