const CONFIG = require('../../config.json');

module.exports = async function(businessNetworkConnection, businessNetworkDefinition, assetID, newOwnerId){
  try {
    let serializer = businessNetworkDefinition.getSerializer();

    let resource = await serializer.fromJSON({
      '$class': `${CONFIG.namespace}.${CONFIG.ownership_transfer_transaction}`,
      'asset': assetID,
      'newOwner': newOwnerId
    });

    console.log("preparing to update owner");
    await businessNetworkConnection.submitTransaction(resource);
    console.log("owner updated to ", newOwnerId);
  } catch (error){
    console.log(error);
  }
}