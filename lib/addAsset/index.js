const CONFIG = require('../../config.json');

module.exports = async function (businessNetworkConnection, assetDetails){
  try {
    const {assetID = (new Date()).getTime().toString(), value, ownerID} = assetDetails;
    const assetRegistry = await businessNetworkConnection.getAssetRegistry(`${CONFIG.namespace}.${CONFIG.asset}`);
    let factory = businessNetworkConnection.getBusinessNetwork().getFactory();
    const owner = factory.newRelationship(CONFIG.namespace, CONFIG.participant, ownerID)
    let asset = factory.newResource(CONFIG.namespace, CONFIG.asset, assetID);
    asset.owner = owner;
    asset.value = value;
    await assetRegistry.add(asset);
    return assetID;
  } catch (error) {
    console.log(error);
  }
};