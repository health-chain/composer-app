const CONFIG = require('../../config.json')

module.exports = async function (businessNetworkConnection, participantDetails) {
    try {
        const {participantID = (new Date()).getTime().toString(), firstName = "FirstName", lastName = "LastName"} = participantDetails;
        let participantRegistry = await businessNetworkConnection.getParticipantRegistry(`${CONFIG.namespace}.${CONFIG.participant}`);
        let factory = businessNetworkConnection.getBusinessNetwork().getFactory();
        let participant = factory.newResource(CONFIG.namespace, CONFIG.participant, participantID);
        participant.firstName = firstName;
        participant.lastName = lastName;
        await participantRegistry.add(participant);
        return participantID;
    } catch (error) {
        console.error(error);
    }
}