# Basic Composer Client APP

Basic composer app made using composer-client module to create Assets and Participants in a connected blockchain network.

# Useful Resources
- https://hyperledger.github.io/composer/latest/applications/node
- https://ibm-blockchain.github.io/develop//api/common-factory#newrelationship
- https://hyperledger.github.io/composer/latest/api/api-doc-index.html
- To understand the scripts file in BNA, visit https://hyperledger.github.io/composer/latest/reference/js_scripts

## Steps to Setup Composer
```sh
npm install composer-cli -g
npm install composer-playground
composer import card -f connection/admin@my-basic-sample.card
```

## Steps to Run Application
```sh
npm install
node lib/app.js
```

## Check Transactions in Composer Playground
```sh
composer-playground
```

# TODO
> [x] Create Participants
> [x] Create Assets with relationship with Participants
> [x] Submit transaction to update value of assets
> [x] Submit transaction to transfer assets from one owner to another