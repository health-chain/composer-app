const express = require('express')
const app = express()

app.post('/api/login', function(req, res) {
  // ...
})

app.post('/api/ping', function(req, res) {
  // ...
})

app.post('/api/logout', function(req, res) {
  // ...
})
